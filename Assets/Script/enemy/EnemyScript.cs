﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private int health = 50;
    private int enemyScore = 5;
    public static int damage;
            
    void OnTriggerEnter2D(Collider2D other)
    {
       
        if (other.gameObject.tag == "PlayerSword")
        {
            health -= Player.Damage;

            if (health <= 0)
            {
                Player.Score += enemyScore;
                Destroy(this.gameObject);
            }
        }

        if (other.gameObject.tag == "Player")
        {
            if (Player.Immor == true)
            {
                damage = 0;
                Boss.BossDamage = 0;
            }
            else
            {
                damage = 6;
                Boss.BossDamage = 15;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Player.Immor == true)
            {
                Destroy(this.gameObject);
            }

        }
    }

}
