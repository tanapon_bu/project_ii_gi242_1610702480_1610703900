﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newCam : MonoBehaviour
{
    public Transform Player;
    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        offset = transform.position - Player.transform.position;
    }

    // Update is called after Update each frame
    void LateUpdate()
    {
        transform.position = Player.transform.position + offset;
    }
}
