﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool NexLevel = false;
    public static int FallDamage = 100;
    public static bool EndGame = false;

    public Text Score;
    public Text ResultScoreText;
    [SerializeField] private GameObject level1;
    [SerializeField] private GameObject level2;
    [SerializeField] private GameObject GameOver;
    [SerializeField] private GameObject Win;
    [SerializeField] private GameObject PauseMenu;

    // Start is called before the first frame update
    void Start()
    {
        SoundManager.Instance.PlayBGM();
    }

    // Update is called once per frame
    void Update()
    {
        SetActive();
        UpdateScore();
    }

    public void SetActive()
    {
        if (NexLevel != false)
        {
            level1.SetActive(false);
            level2.SetActive(true);
        }
        else
        {
            level1.SetActive(true);
            level2.SetActive(false);
        }

        if (Player.Health <= 0 && Player.Lives <= 0)
        {
            Time.timeScale = 0;
            level2.SetActive(false);
            GameOver.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            GameOver.SetActive(false);
        }

        if (EndGame != false)
        {
            Win.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            Win.SetActive(false);
        }

    }
    public void Pause()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);

    }
    public void Resume()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
    }
    public void UpdateScore()
    {
        Score.text = "Score : " + Player.Score;
        ResultScoreText.text = "Your Score = " + Player.Score;
    }
}

