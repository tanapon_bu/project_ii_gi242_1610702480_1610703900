﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene("Level1");
        Time.timeScale = 1;
        GameManager.NexLevel = false;
        GameManager.EndGame = false;

    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Tutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }
    public void ExitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
