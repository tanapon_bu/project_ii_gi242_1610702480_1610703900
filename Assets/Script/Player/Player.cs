﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Player : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip playerSlashSound;
    [SerializeField] float playerSlashSoundVolume = 0.3f;
    [SerializeField] AudioClip playerDied;
    [SerializeField] float playerDiedSoundVolume = 0.3f;
    [SerializeField] AudioClip playerJump;
    [SerializeField] float playerJumpSoundVolume = 0.3f;

    public Animator Animator;
    public Text HPText;
    public Text livesText;
    public Transform SpawnPoint;

    public static int Health;
    public static int Damage;
    public static bool Immor;
    public static int Lives;
    public static int Score;

    private int maxHealth;
    private bool dead;
    private bool isGround;
    private float immortalTime;
    private float speed;


    [SerializeField] private GameObject AttackHitbox;
    [SerializeField] private GameObject GroundReturntofield;
    void Start()
    {
        Init();
        Animator = GetComponent<Animator>();
    }

    void Update()
    {
        
        if (!dead)
        {
            Move();
            Attack();
        }

        UpdateHPText();
        UpdateLivesText();
        Dead();
        immortal();
    }

    void Init()
    {
        dead = false;
        Health = 100;
        Immor = false;
        maxHealth = 100;
        Damage = Random.Range(10, 21);
        Lives = 3;
        speed = 3f;
        Score = 0;
    }
    void immortal()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (!Immor)
            {
                Immor = true;
                Animator.SetTrigger("Player_Imortal");
                Animator.ResetTrigger("Player_Idle");
                Animator.ResetTrigger("Player_Run");

            }

        }
        if (Immor)  
        {
            immortalTime += Time.deltaTime;

            if (immortalTime >= 5)
            {
                Immor = false;
                immortalTime = 0;
                Animator.ResetTrigger("Player_Imortal");
                Animator.ResetTrigger("Player_RunImmortal");

            }
        }
    }

    void Move()
    {
        if (Input.GetKey(KeyCode.A)) 
        {
            if(Immor == true)
            {
                Animator.ResetTrigger("Player_Idle");
                Animator.ResetTrigger("Player_Run");
                Animator.ResetTrigger("Player_Imortal");
                Animator.SetTrigger("Player_RunImmortal");
            }
            else
            {
                Animator.ResetTrigger("Player_Idle");
                Animator.SetTrigger("Player_Run");
            }
            transform.Translate(Vector3.left * speed * Time.deltaTime);
            Flip();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (Immor == true)
            {
                Animator.ResetTrigger("Player_Idle");
                Animator.ResetTrigger("Player_Run");
                Animator.ResetTrigger("Player_Imortal");
                Animator.SetTrigger("Player_RunImmortal");
            }
            else
            {
                Animator.ResetTrigger("Player_Idle");
                Animator.SetTrigger("Player_Run");
            }

            transform.Translate(Vector3.right * speed * Time.deltaTime);
            Flip();
        }
        else
        {
            if(Immor == true)
            {
                Animator.SetTrigger("Player_Imortal");
                Animator.ResetTrigger("Player_RunImmortal");
            }
            else
            {
                Animator.ResetTrigger("Player_Run");
                Animator.SetTrigger("Player_Idle");
            }
        }

        if (Input.GetKeyDown(KeyCode.W) && isGround)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 200);
            Animator.SetTrigger("Player_Jump");
            isGround = false;

            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Jump);
        }
        else
        {
            Animator.ResetTrigger("Player_Jump");
        }
    }

    void Attack() 
    {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Animator.SetTrigger("Player_Attack");
            AttackHitbox.SetActive(true);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Slash);
        }
        else
        {
            Animator.ResetTrigger("Player_Attack");
            AttackHitbox.SetActive(false);
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            AttackHitbox.SetActive(false);
        }
    }

    void Dead() 
    {
        if (Health <= 0)
        {
            Lives -= 1;
            Animator.SetTrigger("Player_Die");
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerDie);

            if (Lives > 0)
            {
              Health =  maxHealth;
            }

            Respawn();
            Immor = true;
            UpdateHPText();
            UpdateLivesText();
        }

        if (Health <= 0 && Lives <= 0)
        {
            Health = 0;
            Lives = 0;
            UpdateHPText();
            UpdateLivesText();
            Animator.SetTrigger("Player_Die");
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerDie);
            Immor = false;
            dead = true;
        }

    }

    void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.gameObject.tag == "ground") 
        {
            isGround = true;
        }

        if (other.gameObject.tag == "GroundCheck" && GroundReturntofield) 
        {
            Respawn();
        }

        if (other.gameObject.tag == "NextLevel")
        {
            GameManager.NexLevel = true;
        }

        if (other.gameObject.tag == "EndGame")
        {
            GameManager.EndGame = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EnemySword")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 20);
            GetComponent<Rigidbody2D>().AddForce(Vector2.left * 200);

            Health -= EnemyScript.damage;
            UpdateHPText();
            UpdateLivesText();

        }
        else if (collision.gameObject.tag == "BossAttack")
        {
            if (!Immor)
            {
                GetComponent<Rigidbody2D>().AddForce(Vector2.up * 20);
                GetComponent<Rigidbody2D>().AddForce(Vector2.left * 200);

                Health -= Boss.BossDamage;
                UpdateHPText();
                UpdateLivesText();
            }

        }

        if (collision.gameObject.tag == "Potion")
        {
            Destroy(collision.gameObject);
        }
    }

    public void Respawn()
    {
        Animator.ResetTrigger("Player_Die");
        this.transform.position = SpawnPoint.position;
    }

    void UpdateHPText()
    {
        HPText.text = "HP : " + Health;

    }
    void UpdateLivesText()
    {
        livesText.text = "Lives : " + Lives;
    }
    void Flip() 
    {
        Vector3 characterSacle = transform.localScale;
        if (Input.GetKeyDown(KeyCode.A))
        {
            characterSacle.x = -1;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            characterSacle.x = 1;
        }
        transform.localScale = characterSacle;
    }
}
