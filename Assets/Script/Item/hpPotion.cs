﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hpPotion : MonoBehaviour
{
    public int hpPoint;


    private void Start()
    {
        hpPoint = 50;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player.Health += hpPoint;
            if (Player.Health > 100)
            {
                Player.Health = 100;
                Player.Lives += 1;

                if (Player.Lives >5)
                {
                    Player.Lives = 5;
                }
            }

            Destroy(this.gameObject);
        }
    }
}
